package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.entity.Livro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroResponse;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.BuscarPorTituloGateway;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.SalvarGateway;

@Service
public class CadastrarLivroUseCaseImpl implements CadastrarLivroUseCase {
	
	private final SalvarGateway<Livro> salvarGateway;
	private final BuscarPorTituloGateway buscarPorTituloGateway;
	
	public CadastrarLivroUseCaseImpl(SalvarGateway<Livro> salvarGateway,
									 BuscarPorTituloGateway buscarPorTituloGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorTituloGateway = buscarPorTituloGateway;
	}

	@Override
	public LivroResponse executar(LivroRequest input) {
		
		LivroResponse response = new LivroResponse();
		response.setAutor(input.getAutor());
		response.setDataPublicacao(input.getDataPublicacao());
		response.setEditora(input.getEditora());
		response.setNumeroPaginas(input.getNumeroPaginas());
		response.setTitulo(input.getTitulo());
		response.setDisponivel(input.getDisponivel());
		
		if (buscarPorTituloGateway.buscarPorTituloLivro(input.getTitulo().toLowerCase().replaceAll("\\s+", "")).isPresent()) {
			response.getResponse().adicionarErro(new ResponseDataErro("Livro ja cadastrado com titulo: " + input.getTitulo(), ListaErroEnum.DUPLICIDADE));
		} else {
			
			Livro livro = new Livro();
			
			livro.setAutor(input.getAutor());
			livro.setDataPublicacao(input.getDataPublicacao());
			livro.setEditora(input.getEditora());
			livro.setNumeroPaginas(input.getNumeroPaginas());
			livro.setTitulo(input.getTitulo().toLowerCase().replaceAll("\\s+", ""));
			livro.setDisponivel(null);
			response.setId(livro.getId());
			salvarGateway.Salvar(livro);
		}
		
		return response;
	}

}
