package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway;

public interface SalvarGateway<T> {

	T Salvar (T t);
	
}
