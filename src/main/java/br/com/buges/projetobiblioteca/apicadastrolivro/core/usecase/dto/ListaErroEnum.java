package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS, DUPLICIDADE, OUTROS, ENTIDADE_NAO_ENCONTRADA;
}
