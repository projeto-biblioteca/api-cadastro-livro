package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto;

public class EmprestimoRequest {
	
	private String titulo;
	private Long disponivel;
	

	public Long getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Long disponivel) {
		this.disponivel = disponivel;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
