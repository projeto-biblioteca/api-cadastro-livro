package br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa.entity.JpaLivroEntity;

public interface JpaLivroRepository extends JpaRepository<JpaLivroEntity, String>{

	Optional<JpaLivroEntity> findByTitulo(String titulo);
}
