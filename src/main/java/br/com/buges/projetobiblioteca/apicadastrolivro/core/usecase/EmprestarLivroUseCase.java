package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoResponse;

public interface EmprestarLivroUseCase extends BaseUseCase<EmprestimoRequest, EmprestimoResponse> {

}
