package br.com.buges.projetobiblioteca.apicadastrolivro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCadastroLivroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCadastroLivroApplication.class, args);
	}

}
