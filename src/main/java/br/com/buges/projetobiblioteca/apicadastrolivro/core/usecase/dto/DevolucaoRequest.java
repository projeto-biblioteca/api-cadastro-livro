package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto;

public class DevolucaoRequest {
	
	private String titulo;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
