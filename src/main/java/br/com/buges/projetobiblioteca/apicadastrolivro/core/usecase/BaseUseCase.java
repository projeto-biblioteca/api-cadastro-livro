package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

public interface BaseUseCase<I, O> {

	O executar (I input);
}
