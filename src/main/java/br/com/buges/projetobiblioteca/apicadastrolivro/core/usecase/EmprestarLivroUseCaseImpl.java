package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.entity.Livro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoResponse;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.BuscarPorTituloGateway;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.SalvarGateway;

@Service
public class EmprestarLivroUseCaseImpl implements EmprestarLivroUseCase {
	
	private final SalvarGateway<Livro> salvarGateway;
	private final BuscarPorTituloGateway buscarPorTituloGateway;
	
	public EmprestarLivroUseCaseImpl(SalvarGateway<Livro> salvarGateway,
									 BuscarPorTituloGateway buscarPorTituloGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarPorTituloGateway = buscarPorTituloGateway;
	}

	@Override
	public EmprestimoResponse executar(EmprestimoRequest input) {
		
		EmprestimoResponse response = new EmprestimoResponse();
		
		Optional<Livro> livroOptional = buscarPorTituloGateway.buscarPorTituloLivro(input.getTitulo().toLowerCase().replaceAll("\\s+", ""));
		
		if(!livroOptional.isPresent()) {
			response.getResponse().adicionarErro(new ResponseDataErro("Nenhum livro cadastrado com titulo: " + input.getTitulo(), ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		} else {
			Livro livro = new Livro();
			
			livro.setAutor(livroOptional.get().getAutor());
			livro.setDataPublicacao(livroOptional.get().getDataPublicacao());
			livro.setEditora(livroOptional.get().getEditora());
			livro.setNumeroPaginas(livroOptional.get().getNumeroPaginas());
			livro.setTitulo(livroOptional.get().getTitulo());
			livro.setDisponivel(input.getDisponivel());
			livro.setId(livroOptional.get().getId());
			
			salvarGateway.Salvar(livro);
			
			response.setAutor(livro.getAutor());
			response.setDataPublicacao(livro.getDataPublicacao());
			response.setEditora(livro.getEditora());
			response.setNumeroPaginas(livro.getNumeroPaginas());
			response.setTitulo(livro.getTitulo());
			response.setDisponivel(input.getDisponivel());
			response.setId(livro.getId());
			
			if(input.getDisponivel() == null) {
				response.getResponse().adicionarInfo("Devolucao realizada com sucesso");
			} else {
				response.getResponse().adicionarInfo("Emprestimo realizado com sucesso para portador do CPF: " + input.getDisponivel());
			}
		}
		
		return response;
	}

}
