package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroResponse;

public interface BuscarPorTituloUseCase extends BaseUseCase<String, LivroResponse> {

}
