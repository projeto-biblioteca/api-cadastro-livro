package br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa;

import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.buges.projetobiblioteca.apicadastrolivro.configuration.mapper.ObjectMapperUtil;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.entity.Livro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.BuscarPorTituloGateway;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.SalvarGateway;
import br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa.entity.JpaLivroEntity;
import br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa.repository.JpaLivroRepository;

@Repository
public class LivroDataProvider implements BuscarPorTituloGateway, SalvarGateway<Livro> {
	
	private final JpaLivroRepository repository;

	public LivroDataProvider(JpaLivroRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<Livro> buscarPorTituloLivro(String titulo) {
		
		JpaLivroEntity entity = repository.findByTitulo(titulo).orElse(null);
		
		if(Objects.nonNull(entity)) {
			return Optional.of(ObjectMapperUtil.convertTo(entity, Livro.class));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Livro Salvar(Livro t) {
		
		JpaLivroEntity entity = repository.save(ObjectMapperUtil.convertTo(t, JpaLivroEntity.class));
				
		return ObjectMapperUtil.convertTo(entity, Livro.class);
	}
}
