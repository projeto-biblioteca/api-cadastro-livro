package br.com.buges.projetobiblioteca.apicadastrolivro.core.entity;

import java.util.Objects;
import java.util.UUID;

public abstract class BaseEntity {
	
	public BaseEntity() {
		this.id = UUID.randomUUID().toString();
	}

	private String id;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		return Objects.equals(id, other.id);
	}	

}
