package br.com.buges.projetobiblioteca.apicadastrolivro.entrypoint.http.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.BaseUseCase;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.BuscarPorTituloUseCase;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.DevolucaoRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.EmprestimoResponse;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroResponse;

@RestController
@RequestMapping("/api/v1/livros")
public class LivroController {
	
	private final BaseUseCase<LivroRequest, LivroResponse> usecase;
	private final BaseUseCase<EmprestimoRequest, EmprestimoResponse> usecaseEmprestimo;
	private final BuscarPorTituloUseCase buscarPorTituloUseCase;

	public LivroController(BaseUseCase<LivroRequest, LivroResponse> uc,
			BaseUseCase<EmprestimoRequest, EmprestimoResponse> ucEmprestimo,
			BuscarPorTituloUseCase buscarPorTituloUseCase) {
		this.usecase = uc;
		this.buscarPorTituloUseCase = buscarPorTituloUseCase;;
		this.usecaseEmprestimo = ucEmprestimo;
	}	

	@CrossOrigin(origins = "http://localhost:4200")
	@SuppressWarnings("deprecation")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<LivroResponse> cadastrarLivro(@RequestBody LivroRequest request) {
		
		LivroResponse response = usecase.executar(request);
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/{titulo}")
	public ResponseEntity<LivroResponse> buscarPorTitulo(@PathVariable("titulo") String titulo) {
		
		LivroResponse response = buscarPorTituloUseCase.executar(titulo);
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/emprestimo")
	public ResponseEntity<EmprestimoResponse> emprestarLivro(@RequestBody EmprestimoRequest request){
		
		EmprestimoResponse response = usecaseEmprestimo.executar(request);
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/devolucao")
	public ResponseEntity<EmprestimoResponse> devolverLivro(@RequestBody DevolucaoRequest request){
		
		EmprestimoRequest update = new EmprestimoRequest();
		
		update.setDisponivel(null);
		update.setTitulo(request.getTitulo());
		
		EmprestimoResponse response = usecaseEmprestimo.executar(update);
		return ResponseEntity.ok(response);
	}
	
//	@GetMapping("/all")
//	public ResponseEntity<ClienteResponse> listarTodosOsClientes() {
//		
//		ClienteResponse response = buscarTodosUseCase.executar("all");
//		return ResponseEntity.ok(response);
//	}

}
