package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway;

import java.util.Optional;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.entity.Livro;


public interface BuscarPorTituloGateway {
	
	Optional<Livro> buscarPorTituloLivro(String titulo);

}
