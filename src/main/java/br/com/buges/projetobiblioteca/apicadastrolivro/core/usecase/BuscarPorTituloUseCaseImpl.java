package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.entity.Livro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ListaErroEnum;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroResponse;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.ResponseDataErro;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.gateway.BuscarPorTituloGateway;

@Service
public class BuscarPorTituloUseCaseImpl implements BuscarPorTituloUseCase {
	
	private final BuscarPorTituloGateway buscarPorTituloGateway;
	
	public BuscarPorTituloUseCaseImpl(BuscarPorTituloGateway buscarPorTituloGateway) {
		this.buscarPorTituloGateway = buscarPorTituloGateway;
	}

	@Override
	public LivroResponse executar(String titulo) {
		
		LivroResponse response = new LivroResponse();
		response.setTitulo(titulo);
		
		Optional<Livro> opLivro = buscarPorTituloGateway.buscarPorTituloLivro(titulo);
		
		if(opLivro.isPresent()) {
			response.setAutor(opLivro.get().getAutor());
			response.setEditora(opLivro.get().getEditora());
			response.setDataPublicacao(opLivro.get().getDataPublicacao());
			response.setNumeroPaginas(opLivro.get().getNumeroPaginas());
			response.setId(opLivro.get().getId());
			response.setDisponivel(opLivro.get().getDisponivel());
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro("Livro não encontrado com titulo:  " + titulo,
					ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		}
		
		return response;
	}

}
