package br.com.buges.projetobiblioteca.apicadastrolivro.core.entity;

import java.time.LocalDate;

public class Livro extends BaseEntity {

	private String titulo;
	private String autor;
	private String editora;
	private LocalDate dataPublicacao;
	private int numeroPaginas;
	private Long disponivel;
	
	public Long getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Long disponivel) {
		this.disponivel = disponivel;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}
	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	public int getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

}
