package br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase;

import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroRequest;
import br.com.buges.projetobiblioteca.apicadastrolivro.core.usecase.dto.LivroResponse;

public interface CadastrarLivroUseCase extends BaseUseCase<LivroRequest, LivroResponse> {

}
