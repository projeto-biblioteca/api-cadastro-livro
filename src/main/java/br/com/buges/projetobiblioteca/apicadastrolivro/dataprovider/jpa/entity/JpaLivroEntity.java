package br.com.buges.projetobiblioteca.apicadastrolivro.dataprovider.jpa.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "livro")
public class JpaLivroEntity extends JpaBaseEntity {

	@Column(name = "titulo", length = 40, nullable = false)
	private String titulo;
	@Column(name = "autor", length = 40, nullable = false)
	private String autor;
	@Column(name = "editora", length = 40, nullable = false)
	private String editora;
	@Column(name = "dataPublicacao", length = 40, nullable = false)
	private LocalDate dataPublicacao;
	@Column(name = "numeroPaginas", length = 40, nullable = false)
	private int numeroPaginas;
	@Column(name = "disponivel", length = 40, nullable = true)
	private Long disponivel;
	
	public Long getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Long disponivel) {
		this.disponivel = disponivel;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}
	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	public int getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}
	
}
